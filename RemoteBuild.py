import sublime
import sublime_plugin
import os.path
import subprocess
import time
import threading


class RemoteBuildDirectoryNotFoundError(Exception):
    pass


class RemoteBuildFileNotFoundError(Exception):
    pass


class RemoteBuildWriteTextCommand(sublime_plugin.TextCommand):
    def run(self, edit, content, clear, at_end):
        if clear:
            region = sublime.Region(0, self.output_view.size())
            self.view.erase(edit, region)
        point = 0
        if at_end:
            point = self.view.size()
        self.view.insert(edit, point, content)


def debug(text=''):
    if sublime.load_settings('Remote Build.sublime-settings').get(
            'debug', False):
        print('RemoteBuild: debug: %s' % text)


def error(text=''):
    print('RemoteBuild: error: %s' % text)


class RemoteBuildCommand():
    def get_path(self, paths):
        if paths is True:
            return self.get_window().active_view().file_name()
        return paths[
            0] if paths else self.get_window().active_view().file_name()

    def get_makefile(self, path):
        if path is None:
            raise RemoteBuildFileNotFoundError(
                'Unable to run commands on an unsaved file')

        makefile = None

        try:
            makefile = SublimeRemoteBuild(path)
        except (RemoteBuildDirectoryNotFoundError):
            pass

        if makefile is None:
            raise RemoteBuildFileNotFoundError('Unable to find Makefile')
        return makefile

    def get_window(self):
        return self.window

    def output_to_panel(self, text, panel_name, syntax=None, show=False):
        syntax = 'Packages/Remote Build/OutputPanel.tmLanguage'
        if not hasattr(self, 'output_panel'):
            self.output_panel = self.get_window().create_output_panel(
                panel_name)
        panel = self.output_panel
        if syntax is not None:
            panel.set_syntax_file(syntax)

        panel.set_read_only(False)
        panel.run_command('remote_build_write_text', {
            'content': text,
            'clear': False,
            'at_end': True
        })
        panel.show(panel.size())
        panel.set_read_only(True)

        if show:
            self.get_window().run_command("show_panel", {
                "panel": "output." + panel_name
            })


def handles_not_found(fn):
    def handler(self, *args, **kwargs):
        try:
            fn(self, *args, **kwargs)
        except (RemoteBuildFileNotFoundError) as handler_not_found_exception:
            (exception) = handler_not_found_exception
            sublime.error_message('RemoteBuild: ' + str(exception))

    return handler


def invisible_when_not_found(fn):
    def handler(self, *args, **kwargs):
        try:
            res = fn(self, *args, **kwargs)
            if res is not None:
                return res
            return True
        except (RemoteBuildFileNotFoundError):
            return False

    return handler


class RemoteBuildCleanCommand(sublime_plugin.WindowCommand,
                              RemoteBuildCommand):
    @handles_not_found
    def run(self, paths=None):
        path = self.get_path(paths)
        project = self.get_window().project_data().get('remote_build', None)
        self.get_makefile(path).clean(
            path if paths else None,
            project=project,
            output_func=self.output_to_panel,
            output_title='remote_build_clean',
            show_dialog=sublime.load_settings('Remote Build.sublime-settings')
            .get('show_dialog', False))

    @invisible_when_not_found
    def is_visible(self, paths=None):
        return True

    @invisible_when_not_found
    def is_enabled(self, paths=None):
        path = self.get_path(paths)
        return path and self.get_makefile(path) is not None


class RemoteBuildBuildCommand(sublime_plugin.WindowCommand,
                              RemoteBuildCommand):
    @handles_not_found
    def run(self, paths=None):
        path = self.get_path(paths)
        project = self.get_window().project_data().get('remote_build', None)
        self.get_makefile(path).build(
            path if paths else None,
            project=project,
            output_func=self.output_to_panel,
            output_title='remote_build_build',
            show_dialog=sublime.load_settings('Remote Build.sublime-settings')
            .get('show_dialog', False))

    @invisible_when_not_found
    def is_visible(self, paths=None):
        return True

    @invisible_when_not_found
    def is_enabled(self, paths=None):
        path = self.get_path(paths)
        return path and self.get_makefile(path) is not None


class RemoteBuildRebuildCommand(sublime_plugin.WindowCommand,
                                RemoteBuildCommand):
    @handles_not_found
    def run(self, paths=None):
        path = self.get_path(paths)
        project = self.get_window().project_data().get('remote_build', None)
        self.get_makefile(path).rebuild(
            path if paths else None,
            project=project,
            output_func=self.output_to_panel,
            output_title='remote_build_rebuild',
            show_dialog=sublime.load_settings('Remote Build.sublime-settings')
            .get('show_dialog', False))

    @invisible_when_not_found
    def is_visible(self, paths=None):
        return True

    @invisible_when_not_found
    def is_enabled(self, paths=None):
        path = self.get_path(paths)
        return path and self.get_makefile(path) is not None


class SublimeRemoteBuild(RemoteBuildCommand):
    def __init__(self, file):
        self.file = file

    def _call_make(self,
                   target=None,
                   project=None,
                   output_func=False,
                   output_title='',
                   show_dialog=True):
        cwd = self.file if os.path.isdir(self.file) else os.path.dirname(
            self.file)
        remote_build_settings = sublime.load_settings(
            'Remote Build.sublime-settings')

        host = remote_build_settings.get('host', '')
        user = remote_build_settings.get('user', '')
        password_option = remote_build_settings.get('password_option', '')
        password = remote_build_settings.get('password', '')
        port_option = remote_build_settings.get('port_option', '')
        port = remote_build_settings.get('port', '')
        prefix_cmd = remote_build_settings.get('prefix_cmd', '')
        remote_path = remote_build_settings.get('remote_path', '')
        local_path = remote_build_settings.get('local_path', '')
        shell_cmd = remote_build_settings.get('shell_cmd', '')
        login_program = remote_build_settings.get('login_program')
        login_program_args = remote_build_settings.get('login_program_args',
                                                       None)

        if project is not None:
            debug('project specific settings for %s' % (project))
            for remote_build_settings in project:
                local_path = remote_build_settings.get('local_path', '')
                if cwd.startswith(local_path):
                    host = remote_build_settings.get('host', host)
                    user = remote_build_settings.get('user', user)
                    password_option = remote_build_settings.get(
                        'password_option', password_option)
                    password = remote_build_settings.get('password', password)
                    port_option = remote_build_settings.get(
                        'port_option', port_option)
                    port = remote_build_settings.get('port', port)
                    prefix_cmd = remote_build_settings.get(
                        'prefix_cmd', prefix_cmd)
                    remote_path = remote_build_settings.get(
                        'remote_path', remote_path)
                    shell_cmd = remote_build_settings.get('shell_cmd', '')
                    login_program = remote_build_settings.get(
                        'login_program', login_program)
                    login_program_args = remote_build_settings.get(
                        'login_program_args', login_program_args)

        if len(user) > 0:
            host = user + '@' + host

        command = shell_cmd
        if target is not None:
            command = command + ' ' + target
        if len(remote_path) > 0:
            command = 'cd \"' + remote_path + '\";' + command
        if len(prefix_cmd) > 0:
            command = prefix_cmd + ';' + command

        args = [
            login_program, login_program_args, host, port_option, port,
            password_option, password, command
        ]
        debug('arguments: %s' % args)

        object = RemoteBuildObject()
        object.args = [i for i in args if i is not None and i != '']
        object.cwd = cwd
        object.target = target
        object.output_func = output_func
        object.output_title = 'Remote Build'
        object.show_dialog = show_dialog
        RemoteBuildQueue.queue.append(object)
        if not RemoteBuildQueue.running:
            RemoteBuildThread().start()

    def clean(self,
              path=None,
              project=None,
              output_func=False,
              output_title='',
              show_dialog=True):
        self._call_make(
            target='clean',
            project=project,
            output_func=output_func,
            output_title=output_title,
            show_dialog=show_dialog)

    def build(self,
              path=None,
              project=None,
              output_func=False,
              output_title='',
              show_dialog=True):
        self._call_make(
            project=project,
            output_func=output_func,
            output_title=output_title,
            show_dialog=show_dialog)

    def rebuild(self,
                path=None,
                project=None,
                output_func=False,
                output_title='',
                show_dialog=True):
        self.clean(path, project, output_func, output_title, False)
        self.build(path, project, output_func, output_title, show_dialog)


class RemoteBuildObject():
    pass


class RemoteBuildQueue:
    def __init__(self):
        self.queue = []
        self.running = False


RemoteBuildQueue = RemoteBuildQueue()


class RemoteBuildThread(threading.Thread):
    def __init__(self):
        RemoteBuildQueue.running = True
        threading.Thread.__init__(self)

    def run(self):
        if len(RemoteBuildQueue.queue) > 0:
            object = RemoteBuildQueue.queue.pop(0)
            RemoteBuildNonInteractiveProcess(
                object.args, object.cwd, object.target).run(
                    object.output_func, object.output_title,
                    object.show_dialog)
            RemoteBuildThread().start()
        RemoteBuildQueue.running = False


class RemoteBuildNonInteractiveProcess():
    def __init__(self, args, cwd=None, target=None):
        self.args = args
        self.cwd = cwd
        self.target = target

    def run(self, output_func=False, output_title='', show_dialog=True):
        startupinfo = None
        if os.name == 'nt':
            startupinfo = subprocess.STARTUPINFO()
            startupinfo.dwFlags |= subprocess.STARTF_USESHOWWINDOW
        proc = subprocess.Popen(
            self.args,
            stdin=subprocess.PIPE,
            stdout=subprocess.PIPE,
            stderr=subprocess.STDOUT,
            startupinfo=startupinfo,
            cwd=self.cwd)

        if output_func != False:
            show = True
            proc.poll()
            while proc.returncode is None:
                output = proc.stdout.readline().decode().replace(
                    '\r\n', '\n').rstrip(' \n\r')
                if len(output) > 0:
                    output_func(output + '\n', output_title, show=show)
                    show = False
                proc.poll()
            output = proc.stdout.read().decode().replace('\r\n',
                                                         '\n').rstrip(' \n\r')
            if len(output) > 0:
                output_func(output + '\n', output_title)
            debug('returncode: %d' % proc.returncode)
            if proc.returncode == 0:
                output_func(('make: %ssuccessful\n' %
                             (self.target + " "
                              if self.target is not None else "")),
                            output_title)
                if show_dialog:
                    sublime.message_dialog(
                        'make: %ssuccessful\n' %
                        (self.target + " " if self.target is not None else ""))
            else:
                output_func(('make: %sfailed\n' %
                             (self.target + " "
                              if self.target is not None else "")),
                            output_title)
                sublime.error_message('make: %sfailed\n' %
                                      (self.target + " "
                                       if self.target is not None else ""))
        else:
            output = proc.stdout.read().decode().replace('\r\n',
                                                         '\n').rstrip(' \n\r')
            debug('returncode: %d' % proc.returncode)
            if proc.returncode == 0:
                output = output + \
                    ('make: %ssuccessful\n' %
                     (self.target + " " if self.target is not None else ""))
                if show_dialog:
                    sublime.message_dialog(
                        'make: %ssuccessful\n' %
                        (self.target + " " if self.target is not None else ""))
            else:
                output = output + \
                    ('make: %sfailed\n' %
                     (self.target + " " if self.target is not None else ""))
                sublime.error_message('make: %sfailed\n' %
                                      (self.target + " "
                                       if self.target is not None else ""))

            if len(output) > 0:
                return output

        return None
