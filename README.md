Sublime RemoteBuild
============
Sublime Text 3 package to allow a build to be performed on a remote server.

Features
--------
- Run make all on a remote server
- Run make clean on a remote server

Screenshots
-----------

<!--img alt="File Context Menu" src="https://raw.github.com/brianhornsby/www_brianhornsby_com/master/img/sublime_make_file_context_menu.png" height="128"/-->

Installation
------------
Download the Sublime RemoteBuild zip file and extract into Sublime Text packages directory. Depending on your system you may have to update the cvs_path setting to point at the correct CVS binary.

Usage
-----
The implemented RemoteBuild commands can be accessed from the context menu of the currently open file or a file/folder in the side bar.

**Default**: ???.

Settings
--------
The default settings can be viewed by accessing the Preferences > Package Settings > RemoteBuild > Settings – Default menu entry. To ensure settings are not lost when the package is upgraded, make sure all edits are saved to Settings – User .

**debug**: Set to true if debug messages should be printed to the console. Default: false

License
-------
Sublime RemoteBuild is licensed under the [MIT license](https://raw.github.com/brianhornsby/sublime_cvs/master/LICENSE.txt).
